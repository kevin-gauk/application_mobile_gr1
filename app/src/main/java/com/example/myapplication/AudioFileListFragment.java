package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.databinding.AudioFileListFragmentBinding;


import java.util.ArrayList;
import java.util.List;


public class AudioFileListFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        AudioFileListFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.audio_file_list_fragment,container,false);
        binding.audioFileList.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));

        /*List<AudioFile> musicList = new ArrayList<AudioFile>();

        AudioFile list1 = new AudioFile("Talking To Myself","/Download","Linkin Park","One More Light","Pop",2003,231);
        AudioFile list2 = new AudioFile("Have A Nice Day","/Download","World Order","HAVE A NICE DAY","Pop",2014,241);

        musicList.add(list1);
        musicList.add(list2);*/

        MainActivity activity = (MainActivity) getActivity();
        List<AudioFile> myMusicList = activity.getMyData();

        AudioFileListAdapter testAudioFileListAdapter = new AudioFileListAdapter(myMusicList);


        binding.audioFileList.setAdapter(testAudioFileListAdapter);
        someFunction();
        return binding.getRoot();
    }


    private MyListener myListener;
    public void setMyListener(MyListener listener){
        myListener = listener;
    }

    public void someFunction() {
        myListener.onDoneSomething();
    }
}