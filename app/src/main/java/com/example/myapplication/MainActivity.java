package com.example.myapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.databinding.ActivityMainBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    public TextView t1;

    private TextView mTextMessage;

    List<AudioFile> musicList = new ArrayList<AudioFile>();

    MyListener listener = new MyListener() {
        @Override
        public void onDoneSomething() {
            System.out.println("blabla");
        }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        private MenuItem item;

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            this.item = item;
            switch (item.getItemId()) {
                case R.id.menu_previous:
                    mTextMessage.setText("You clicked on previous");
                    return true;
                case R.id.menu_play:
                    mTextMessage.setText("You clicked on play");

                    return true;
                case R.id.menu_next:
                    mTextMessage.setText("You clicked on next");
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);


        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.menu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        ContentResolver contentResolver = getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI; // La carte SD
        String[] projection = {MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.TITLE,MediaStore.Audio.Media.ARTIST,MediaStore.Audio.Media.ALBUM,MediaStore.Audio.Media.DURATION}; //chemin du fichier, titre, artist
        Cursor cursor = contentResolver.query(uri,projection,null,null,null);
        if (cursor != null && cursor.moveToFirst())
        {
            int songTitle = cursor.getColumnIndex(projection[1]);
            int songArtist = cursor.getColumnIndex(projection[2]);
            int songAlbum = cursor.getColumnIndex(projection[3]);
            int songDuration = cursor.getColumnIndex(projection[4]);
            do {
                System.out.println(cursor.getInt(songDuration));
                AudioFile music = new AudioFile(cursor.getString(songTitle),"",cursor.getString(songArtist),cursor.getString(songAlbum),"",0,cursor.getInt(songDuration)/1000);
                musicList.add(music);//add song to list

            }while (cursor.moveToNext());
        }


        AudioFileListAdapter audioFileListAdapter = new AudioFileListAdapter(musicList);

        AudioFileListFragment fragment = new AudioFileListFragment();

        transaction.replace(R.id.fragment_container,fragment);
        fragment.setMyListener(listener);

        transaction.commit();
    }

    public List<AudioFile> getMyData() {
        return musicList;
    }


    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private static final int REQUEST_PERMISSIONS = 12345;
    private static final int PERMISSIONS_COUNT = 1;

    @SuppressLint("NewApi")
    private boolean arePermissionsDenied(){
        for (int i = 0 ; i<PERMISSIONS_COUNT ; i++){
            if (checkSelfPermission(PERMISSIONS[i]) != PackageManager.PERMISSION_GRANTED){
                return true;
            }
        }
        return false;
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(arePermissionsDenied()){
            ((ActivityManager) (this.getSystemService(ACTIVITY_SERVICE))).clearApplicationUserData();
            recreate();
        }else{
            onResume();
        }
    }

    private boolean isMusicPlayerInit;

    @Override
    protected void onResume(){
        super.onResume();
        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.M && arePermissionsDenied()){
            requestPermissions(PERMISSIONS,REQUEST_PERMISSIONS);
            return;
        }

        if(!isMusicPlayerInit){
            showStartup();
            isMusicPlayerInit = true;
        }
    }
}